import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class SendMailSSL {

  private String email;
  private String password;
  private String server;
  private String port;
  private String mes;
  private String subject;
  private String recipient;

  public SendMailSSL(String[] parameters) {
    this.email = parameters[0];
    this.password = parameters[1];
    this.server = parameters[2];
    this.port = parameters[3];
    this.mes = parameters[4];
    this.subject = parameters[5];
    this.recipient = parameters[6];
  }

  public void SendMail() {
    Properties props = new Properties();
    props.put("mail.smtp.host", server);
    props.put("mail.smtp.socketFactory.port", port);
    props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
    props.put("mail.smtp.auth", "true");
    props.put("mail.smtp.port", port);
    Session session = Session.getDefaultInstance(props,
        new javax.mail.Authenticator() {
          @Override
          protected PasswordAuthentication getPasswordAuthentication() {
            return new PasswordAuthentication(email, password);
          }
        });

    try {
      Message message = new MimeMessage(session);
      message.setFrom(new InternetAddress(email));
      message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipient));
      message.setSubject(subject);
      message.setText(mes);
      Transport.send(message);
      System.out.println("Done");
    } catch (MessagingException e) {
      throw new RuntimeException(e);
    }
  }
}
