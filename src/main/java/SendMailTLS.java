import java.util.Properties;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class SendMailTLS {

  private String email;
  private String password;
  private String server;
  private String port;
  private String mes;
  private String subject;
  private String recipient;

  public SendMailTLS(String[] parameters) {
    this.email = parameters[0];
    this.password = parameters[1];
    this.server = parameters[2];
    this.port = parameters[3];
    this.mes = parameters[4];
    this.subject = parameters[5];
    this.recipient = parameters[6];
  }

  public void SendMail() {
    Properties prop = new Properties();
    prop.put("mail.smtp.auth", true);
    prop.put("mail.smtp.starttls.enable", "true");
    prop.put("mail.smtp.host", server);
    prop.put("mail.smtp.port", port);

    Session session = Session.getInstance(prop, new Authenticator() {
      @Override
      protected PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication(email, password);
      }
    });
    Message message = new MimeMessage(session);
    try {
      message.setFrom(new InternetAddress(email));
      message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipient));
      message.setSubject(subject);
      String msg = mes;
      MimeBodyPart mimeBodyPart = new MimeBodyPart();
      mimeBodyPart.setContent(msg, "text/html");
      Multipart multipart = new MimeMultipart();
      multipart.addBodyPart(mimeBodyPart);
      message.setContent(multipart);
      Transport.send(message);
      System.out.println("Done");
    } catch (MessagingException e) {
      e.printStackTrace();
    }
  }
}
