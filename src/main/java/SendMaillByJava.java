import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class SendMaillByJava {

  public static void main(String[] args) {
    // The name of the file to open.
    String fileName = "C://temp.txt";

    // This will reference one line at a time
    String line = null;

    try {
      // FileReader reads text files in the default encoding.
      FileReader fileReader = new FileReader(fileName);
      // Always wrap FileReader in BufferedReader.
      BufferedReader bufferedReader = new BufferedReader(fileReader);
      line = bufferedReader.readLine();
      System.out.println(line);
      assert line != null;
      String[] parameters = line.split(";");
      if (parameters[parameters.length - 1].equals("TLS")) {
        SendMailTLS emailSender = new SendMailTLS(parameters);
        emailSender.SendMail();
      } else if (parameters[parameters.length - 1].equals("SSL")) {
        SendMailSSL emailSender = new SendMailSSL(parameters);
        emailSender.SendMail();

      }
      // Always close files.
      bufferedReader.close();
    } catch (FileNotFoundException ex) {
      System.out.println(
          "Unable to open file '" +
              fileName + "'");
    } catch (IOException ex) {
      System.out.println(
          "Error reading file '"
              + fileName + "'");
      // Or we could just do this:
      // ex.printStackTrace();
    }
  }
}
